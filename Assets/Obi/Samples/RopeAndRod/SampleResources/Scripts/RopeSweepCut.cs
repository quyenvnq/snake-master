﻿using System.Linq;
using UnityEngine;
using Obi;

[RequireComponent(typeof(ObiRope))]
public class RopeSweepCut : MonoBehaviour
{
    public Camera cam;

    private ObiRope _rope;
    private LineRenderer _lineRenderer;
    private Vector3 _cutStartPosition;

    private void Awake()
    {
        _rope = GetComponent<ObiRope>();

        AddMouseLine();
    }

    private void OnDestroy()
    {
        DeleteMouseLine();
    }

    private void AddMouseLine()
    {
        var line = new GameObject("Mouse Line");
        _lineRenderer = line.AddComponent<LineRenderer>();
        _lineRenderer.startWidth = 0.005f;
        _lineRenderer.endWidth = 0.005f;
        _lineRenderer.numCapVertices = 2;
        _lineRenderer.sharedMaterial = new Material(Shader.Find("Unlit/Color")) {color = Color.cyan};
        _lineRenderer.enabled = false;
    }

    private void DeleteMouseLine()
    {
        if (_lineRenderer != null)
            Destroy(_lineRenderer.gameObject);
    }

    private void LateUpdate()
    {
        // do nothing if we don't have a camera to cut from.
        if (cam == null) return;

        // process user input and cut the rope if necessary.
        ProcessInput();
    }

    /**
     * Very simple mouse-based input. Not ideal for multitouch screens as it only supports one finger, though.
     */
    private void ProcessInput()
    {
        // When the user clicks the mouse, start a line cut:
        if (Input.GetMouseButtonDown(0))
        {
            _cutStartPosition = Input.mousePosition;
            _lineRenderer.SetPosition(0,
                cam.ScreenToWorldPoint(new Vector3(_cutStartPosition.x, _cutStartPosition.y, 0.5f)));
            _lineRenderer.enabled = true;
        }

        if (_lineRenderer.enabled)
            _lineRenderer.SetPosition(1,
                cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0.5f)));

        // When the user lifts the mouse, proceed to cut.
        if (!Input.GetMouseButtonUp(0)) return;
        ScreenSpaceCut(_cutStartPosition, Input.mousePosition);
        _lineRenderer.enabled = false;
    }


    /**
     * Cuts the rope using a line segment, expressed in screen-space.
     */
    private void ScreenSpaceCut(Vector2 lineStart, Vector2 lineEnd)
    {
        // keep track of whether the rope was cut or not.
        var cut = false;

        // iterate over all elements and test them for intersection with the line:
        foreach (var t in from t in _rope.elements
            let screenPos1 = cam.WorldToScreenPoint(_rope.solver.positions[t.particle1])
            let screenPos2 = cam.WorldToScreenPoint(_rope.solver.positions[t.particle2])
            where SegmentSegmentIntersection(screenPos1, screenPos2, lineStart, lineEnd, out _, out _)
            select t)
        {
            cut = true;
            _rope.Tear(t);
        }

        // If the rope was cut at any point, rebuilt constraints:
        if (cut) _rope.RebuildConstraintsFromElements();
    }

    /**
     * line segment 1 is AB = A+r(B-A)
     * line segment 2 is CD = C+s(D-C)
     * if they intesect, then A+r(B-A) = C+s(D-C), solving for r and s gives the formula below.
     * If both r and s are in the 0,1 range, it meant the segments intersect.
     */
    private static bool SegmentSegmentIntersection(Vector2 a, Vector2 b, Vector2 c, Vector2 d, out float r, out float s)
    {
        var dNum = (b.x - a.x) * (d.y - c.y) - (b.y - a.y) * (d.x - c.x);
        var rNum = (a.y - c.y) * (d.x - c.x) - (a.x - c.x) * (d.y - c.y);
        var sNum = (a.y - c.y) * (b.x - a.x) - (a.x - c.x) * (b.y - a.y);

        if (Mathf.Approximately(rNum, 0) || Mathf.Approximately(dNum, 0))
        {
            r = -1;
            s = -1;
            return false;
        }

        r = rNum / dNum;
        s = sNum / dNum;
        return r >= 0 && r <= 1 && s >= 0 && s <= 1;
    }
}