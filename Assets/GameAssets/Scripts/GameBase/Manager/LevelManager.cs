﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase.Helper;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Manager
{
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] private List<GameObject> levels = new List<GameObject>();
        [SerializeField] private Transform parent;
        [ReadOnly] public int currentLevel;

        private void Awake()
        {
            currentLevel = UserDataManager.Instance.userDataSave.level;
            LoadLevel();
        }

        private void LoadLevel()
        {
            if (levels.Count <= 0) return;
            if (currentLevel > levels.Count - 1) currentLevel = levels.Count - 1;
            if (levels[currentLevel] == null) return;
            SpawnerHelper.CreateSpawner(Vector3.zero, parent, prefab: levels[currentLevel]);
        }
    }
}