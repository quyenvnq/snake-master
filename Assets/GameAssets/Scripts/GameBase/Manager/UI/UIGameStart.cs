﻿using GameAssets.Scripts.GameBase.Base;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Manager.UI
{
    public class UIGameStart : BaseUI
    {
        [SerializeField] private Button btnSnakeNormal;
        [SerializeField] private Button btnSnakeRope;

        protected override void Start()
        {
            base.Start();
            if (btnSnakeNormal != null)
            {
                btnSnakeNormal.onClick.AddListener(() => SceneManager.LoadSceneAsync("GamePlay"));
            }

            if (btnSnakeRope != null)
            {
                btnSnakeRope.onClick.AddListener(() => SceneManager.LoadSceneAsync("GamePlay-Rope"));
            }
        }
    }
}