﻿using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Enum;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Manager.UI
{
    public class UIPlay : BaseUI
    {
        [SerializeField] private Button btnTapToStart;

        protected override void Start()
        {
            base.Start();
            btnTapToStart.onClick.AddListener(() =>
            {
                UIManager.Instance.HideAllAndShowUI(UIType.UIGameStart);
                GameManager.Instance.SetGameState(GameState.Playing);
            });
        }
    }
}