﻿using GameAssets.Scripts.GameBase.Base;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Manager.UI
{
    public class UIGameComplete : BaseUI
    {
        [SerializeField] private Button btnNextLevel;

        protected override void Start()
        {
            base.Start();
            btnNextLevel.onClick.AddListener(() =>
            {
                UserDataManager.Instance.LevelUp();
                SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
            });
        }
    }
}