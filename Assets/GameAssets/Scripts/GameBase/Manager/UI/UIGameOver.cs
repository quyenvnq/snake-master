﻿using GameAssets.Scripts.GameBase.Base;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameBase.Manager.UI
{
    public class UIGameOver : BaseUI
    {
        [SerializeField] private Button btnReplay;

        protected override void Start()
        {
            base.Start();
            btnReplay.onClick.AddListener(() => { SceneManager.LoadScene(SceneManager.GetActiveScene().name); });
        }
    }
}