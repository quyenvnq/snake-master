﻿using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Enum;
using GameAssets.Scripts.GameBase.GamePlay;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Manager
{
    public class GameManager : BaseSingleton<GameManager>
    {
        [ReadOnly] [SerializeField] private GameState currentState;
        [ReadOnly] [SerializeField] private List<BaseCharacter> characters = new List<BaseCharacter>();

        public Camera Camera { get; private set; }
        
        protected override void Awake()
        {
            base.Awake();
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;

            Camera = FindObjectOfType<Camera>();
            AddBaseCharacter(FindObjectsOfType<BaseCharacter>());
        }

        public void AddBaseCharacter(params BaseCharacter[] character)
        {
            characters.AddRange(character);
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            foreach (var x in characters)
            {
                x.CurrentState?.UpdateState();
            }
        }

        public void SetGameState(GameState gameState)
        {
            if (currentState == gameState) return;
            currentState = gameState;
            Debug.Log($"GAME STATE ===> {gameState}");
        }

        public bool IsGameState(GameState state)
        {
            return currentState == state;
        }

        public void OnWin()
        {
            SetGameState(GameState.Win);
            UIManager.Instance.HideAllAndShowUI(UIType.UIGameComplete);
            Player.Instance.ChangeState();
        }

        public void OnLose()
        {
            SetGameState(GameState.Lose);
            UIManager.Instance.HideAllAndShowUI(UIType.UIGameOver);
            Player.Instance.ChangeCharacterState(CharacterState.Death);
        }
    }
}