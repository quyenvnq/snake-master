﻿using System;
using System.Globalization;
using GameAssets.Scripts.GameBase.Base;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Manager
{
    [Serializable]
    public class UserDataSave
    {
        public int level;
        [ReadOnly] public int bestLevel;
        [ReadOnly] public int bestScore;
        [ReadOnly] public int bestScoreEndless;

        public int diamond;
        public int money;

        public bool sound;
        public bool music;
        public bool vibrate;

        [ReadOnly] public string lastOpenDay;
        [ReadOnly] public string versionCode;
    }

    public class UserDataManager : BaseSingleton<UserDataManager>
    {
        [ReadOnly] [SerializeField] private int currentLevel;

        private const string SAVE_DATA = "UserSaveData";
        public UserDataSave userDataSave;

        protected override void Awake()
        {
            base.Awake();
            if (GetDataFromSave() == null) ResetUserData();
            userDataSave = GetDataFromSave();
        }

        private static UserDataSave GetDataFromSave()
        {
            var json = PlayerPrefs.GetString(SAVE_DATA);
            return JsonUtility.FromJson<UserDataSave>(json);
        }

        private void Save()
        {
            currentLevel = userDataSave.level;
            var json = JsonUtility.ToJson(userDataSave);

            PlayerPrefs.SetString(SAVE_DATA, json);
            PlayerPrefs.Save();
        }

        [Button("Clear User Data")]
        private void ClearUserData()
        {
            PlayerPrefs.DeleteAll();
        }

        private void ResetUserData()
        {
            userDataSave = new UserDataSave
            {
                level = 1,
                bestScore = 0,
                bestScoreEndless = 0,
                sound = true,
                music = true,
                vibrate = true,
                diamond = 0,
                money = 0,
                lastOpenDay = DateTime.Now.ToString(CultureInfo.InvariantCulture)
            };

            Save();
        }

        public void SetBestScoreLevel(int score)
        {
            if (score > userDataSave.bestScore) userDataSave.bestScore = score;
            Save();
        }

        public void SetBestScoreEndless(int score)
        {
            if (score > userDataSave.bestScoreEndless) userDataSave.bestScoreEndless = score;
            Save();
        }

        public void LevelUp()
        {
            userDataSave.level++;
            Save();
        }

        public void AddMoney(int value)
        {
            userDataSave.money += value;
            Save();
        }

        public void AddDiamond(int value)
        {
            userDataSave.diamond += value;
            Save();
        }

        public void SetBestLevel(int level)
        {
            if (level > userDataSave.bestLevel) userDataSave.bestLevel = level;
            Save();
        }
    }
}