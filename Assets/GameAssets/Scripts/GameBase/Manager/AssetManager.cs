﻿using GameAssets.Scripts.GameBase.Base;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Manager
{
    public class AssetManager : BaseSingleton<AssetManager>
    {
        [Space] [Header("Sprite")] public Sprite uiSoundOn;
        public Sprite uiSoundOff;
        public Sprite uiMusicOn;
        public Sprite uiMusicOff;
        public Sprite uiHapticOn;
        public Sprite uiHapticOff;
        public Sprite uiUseWatchAds;
        public Sprite uiUseByMoney;
        public Sprite uiUseByDiamond;
        public Sprite uiBtnBuyColor;
        public Sprite uiBtnBought;
    }
}