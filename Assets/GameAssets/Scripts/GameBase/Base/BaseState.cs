﻿using GameAssets.Scripts.GameBase.Interface;

namespace GameAssets.Scripts.GameBase.Base
{
    public class BaseState : IState
    {
        public BaseCharacter Character { get; set; }

        public virtual void StartState()
        {
        }

        public virtual void UpdateState()
        {
        }

        public virtual void ExitState()
        {
        }
    }
}