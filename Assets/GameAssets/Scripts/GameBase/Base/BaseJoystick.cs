﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace GameAssets.Scripts.GameBase.Base
{
    public class BaseJoystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        [SerializeField] private RectTransform bgJoystick;
        [SerializeField] private bool autoHideJoystick;

        public virtual void OnPointerDown(PointerEventData eventData)
        {
        }

        public virtual void OnPointerUp(PointerEventData eventData)
        {
            if (autoHideJoystick)
            {
                bgJoystick.gameObject.SetActive(false);
            }
        }

        public virtual void OnDrag(PointerEventData eventData)
        {
        }
    }
}