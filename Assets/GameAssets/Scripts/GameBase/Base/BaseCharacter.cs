﻿using System;
using GameAssets.Scripts.GameBase.Enum;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.GameBase.Interface;
using GameAssets.Scripts.GameBase.Manager;
using GameAssets.Scripts.GameBase.Manager.UI;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Base
{
    public class BaseCharacter : MonoBehaviour
    {
        [SerializeField] protected float moveSpeed;
        [ReadOnly] public CharacterState characterState;

        protected Collider Collider;
        protected Rigidbody Rigidbody;
        protected Animator Animator;

        protected UIGameStart UIGameStart;
        public IState CurrentState;

        private bool IsMove { get; set; }
        private bool IsAttack { get; set; }
        public bool IsAlive { get; private set; } = true;

        private static readonly int State = Animator.StringToHash("State");

        #region Virtual Monobehaviour

        protected virtual void Awake()
        {
            Collider = GetComponent<SphereCollider>();
            Rigidbody = GetComponent<Rigidbody>();
            UIGameStart = UIManager.Instance.GetUI<UIGameStart>();
        }

        protected virtual void OnEnable()
        {
        }

        protected virtual void OnDisable()
        {
        }

        protected virtual void Start()
        {
        }

        protected virtual void Update()
        {
        }

        protected virtual void FixedUpdate()
        {
        }

        protected virtual void LateUpdate()
        {
        }

        protected virtual void OnCollisionEnter(Collision other)
        {
        }

        protected virtual void OnCollisionStay(Collision other)
        {
        }

        protected virtual void OnCollisionExit(Collision other)
        {
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
        }

        protected virtual void OnTriggerStay(Collider other)
        {
        }

        protected virtual void OnTriggerExit(Collider other)
        {
        }

        protected virtual void OnDestroy()
        {
        }

        #endregion

        public void ChangeState(IState newState = null)
        {
            if ((CurrentState?.ToString() ?? "Null").Equals(newState?.ToString() ?? "Null"))
            {
                Rigidbody.ResetVelocity();
                return;
            }

            CurrentState?.ExitState();
            CurrentState = newState;

            if (CurrentState == null)
            {
                Rigidbody.ResetVelocity();
                return;
            }

            Debug.Log($"STATE ===> {newState}");
            CurrentState.Character = this;
            CurrentState.StartState();
        }

        public bool ChangeCharacterState(CharacterState newState)
        {
            if (!IsAlive) return false;
            characterState = newState;
            Debug.Log($"CHARACTER STATE ===> {newState}");

            switch (newState)
            {
                case CharacterState.Idle:
                    IsMove = false;
                    IsAlive = true;
                    IsAttack = false;
                    break;

                case CharacterState.Move:
                    IsMove = true;
                    IsAttack = false;
                    IsAlive = true;
                    break;

                case CharacterState.Attack:
                    IsMove = false;
                    IsAttack = true;
                    IsAlive = true;
                    break;

                case CharacterState.Death:
                    IsMove = false;
                    IsAttack = false;
                    IsAlive = false;

                    ChangeState();
                    break;

                case CharacterState.NonAttack:
                    IsAttack = false;
                    break;

                case CharacterState.Respawner:
                    IsAlive = true;
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(newState), newState, null);
            }

            return true;
        }
    }
}