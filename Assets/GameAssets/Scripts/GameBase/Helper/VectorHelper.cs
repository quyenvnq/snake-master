﻿using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class VectorHelper
    {
        public static bool Distance(this Vector3 a, Vector3 b, float distance)
        {
            return (a - b).sqrMagnitude < distance * distance;
        }
    }
}