﻿using System.Collections.Generic;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class GameObjectHelper
    {
        public static void IgnoreRaycast(this GameObject go)
        {
            go.layer = LayerMask.NameToLayer("Ignore Raycast");
        }

        public static void CircleFormation(this GameObject go, List<GameObject> children)
        {
        }
    }
}
