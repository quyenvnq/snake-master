﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    [RequireComponent(typeof(SpawnerHelper))]
    public class PoolingHelper : MonoBehaviour
    {
        [Header("Prefabs")] public List<GameObject> prefabs = new List<GameObject>();

        private readonly Dictionary<string, Queue<GameObject>> _pooling =
            new Dictionary<string, Queue<GameObject>>();
        
        public GameObject GetObject(GameObject go, Transform parent)
        {
            if (!_pooling.TryGetValue(go.name, out var objectList)) return CreateNewObject(go, parent);
            if (objectList.Count == 0) return CreateNewObject(go, parent);

            var clone = objectList.Dequeue();
            clone.SetActive(true);
            return clone;
        }

        private static GameObject CreateNewObject(GameObject go, Transform parent)
        {
            var clone = Instantiate(go, parent);
            clone.name = go.name;
            return clone;
        }

        public void ReturnObject(GameObject go, float delay)
        {
            StartCoroutine(DestroyObject(go, delay));
        }

        private IEnumerator DestroyObject(GameObject go, float delay)
        {
            if (go == null) yield break;
            yield return new WaitForSeconds(delay);
            ReturnObject(go);
        }

        private void ReturnObject(GameObject go)
        {
            if (_pooling.TryGetValue(go.name, out var objectList))
            {
                objectList.Enqueue(go);
            }
            else
            {
                var newQueue = new Queue<GameObject>();
                newQueue.Enqueue(go);
                _pooling.Add(go.name, newQueue);
            }

            go.SetActive(false);
        }
    }
}