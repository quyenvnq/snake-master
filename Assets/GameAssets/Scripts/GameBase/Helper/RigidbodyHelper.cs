﻿using System;
using GameAssets.Scripts.GameBase.Manager;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.Helper
{
    public static class RigidbodyHelper
    {
        public static void SetAffectGravity(this Rigidbody rb, Collider c, bool active)
        {
            rb.isKinematic = active;
            c.isTrigger = active;
        }

        public static void Knockback(this Rigidbody rb, Vector3 direction, float thrustForce)
        {
            rb.AddForce(-direction * thrustForce);
        }

        public static void ResetVelocity(this Rigidbody rb)
        {
            rb.velocity = Vector3.zero;
        }

        public static void MoveWithVelocity(this Rigidbody rb, Vector3 direction, float moveSpeed)
        {
            rb.velocity = direction * (Time.fixedDeltaTime * moveSpeed);
        }

        public static void MoveForward(this Rigidbody rb, Transform origin, float moveSpeed)
        {
            rb.MoveWithVelocity(origin.forward, moveSpeed);
        }

        public static void MoveToTarget(this Rigidbody rb, Transform origin, Transform target, float moveSpeed)
        {
            var position = origin.position;
            rb.MovePosition(position + (target.position - position) * moveSpeed * Time.fixedDeltaTime);
        }

        // Return direction to move use mouse or touch
        public static Vector3 MoveDirection(this Rigidbody rb, Transform origin, Action onBegin = null,
            Action onMoving = null, Action onEnded = null)
        {
            var direction = Vector3.zero;
            switch (Application.platform)
            {
                case RuntimePlatform.IPhonePlayer:
                case RuntimePlatform.Android:
                    if (Input.touchCount > 0)
                    {
                        var touch = Input.GetTouch(0);
                        switch (touch.phase)
                        {
                            case TouchPhase.Began:
                                direction = ScreenPositionInClick(touch.position);
                                onBegin?.Invoke();
                                break;

                            case TouchPhase.Moved:
                            case TouchPhase.Stationary:
                                direction = ScreenPositionInClick(touch.position);
                                onMoving?.Invoke();
                                break;

                            case TouchPhase.Ended:
                                rb.ResetVelocity();
                                onEnded?.Invoke();
                                break;

                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }

                    break;

                case RuntimePlatform.WindowsEditor:
                    if (Input.GetMouseButtonDown(0))
                    {
                        direction = ScreenPositionInClick(Input.mousePosition);
                        onBegin?.Invoke();
                    }
                    else if (Input.GetMouseButton(0))
                    {
                        direction = ScreenPositionInClick(Input.mousePosition);
                        onMoving?.Invoke();
                    }
                    else if (Input.GetMouseButtonUp(0))
                    {
                        rb.ResetVelocity();
                        onEnded?.Invoke();
                    }

                    break;
            }

            return direction - origin.position;
        }

        private static Vector3 ScreenPositionInClick(Vector3 position)
        {
            return !Physics.Raycast(GameManager.Instance.Camera.ScreenPointToRay(position),
                out var hit)
                ? Vector3.zero
                : new Vector3(hit.point.x, 0f, hit.point.z);
        }
    }
}