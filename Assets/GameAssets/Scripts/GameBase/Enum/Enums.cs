﻿namespace GameAssets.Scripts.GameBase.Enum
{
    public enum TimeScale
    {
        Player = 1,
        Monster = 2,
        Global = 3
    }

    public enum CharacterState
    {
        Idle = 0,
        Move = 1,
        Attack = 2,
        NonAttack = 3,
        Death = 4,
        Respawner = 5
    }
    
    public enum GameState
    {
        Lobby,
        Loading,
        Pause,
        PreparePlay,
        Playing,
        Lose,
        Win
    }

    public enum UIType
    {
        None,
        UILobby,
        UILoading,
        UIPlay,
        UIGameStart,
        UIGameComplete,
        UIGameOver,
        UIStore,
        UIShop
    }
}