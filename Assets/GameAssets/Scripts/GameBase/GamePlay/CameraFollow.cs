﻿using DG.Tweening;
using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Helper;
using UnityEngine;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    public class CameraFollow : BaseSingleton<CameraFollow>
    {
        [SerializeField] private ParticleSystem confettiFx;
        [Range(0f, 1f)] [SerializeField] private float lerpSpeed = 0.25f;

        private Player _player;
        private Vector3 _originOffset;

        protected override void Start()
        {
            base.Start();
            _player = Player.Instance;
            if (_player == null) return;
            _originOffset = transform.position - _player.transform.position;
        }

        protected override void LateUpdate()
        {
            if (_player == null || !_player.IsAlive) return;
            transform.DOMove(_player.transform.position + _originOffset, lerpSpeed);
        }

        public void EnableConfetti()
        {
            if (confettiFx == null) return;
            confettiFx.SetActive();
        }
    }
}