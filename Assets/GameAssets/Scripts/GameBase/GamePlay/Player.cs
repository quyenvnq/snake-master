﻿using GameAssets.Scripts.GameBase.Base;
using GameAssets.Scripts.GameBase.Enum;
using GameAssets.Scripts.GameBase.Helper;
using GameAssets.Scripts.GameBase.Manager;
using UnityEngine;
using System.Collections.Generic;
using Obi;
using Sirenix.OdinInspector;

namespace GameAssets.Scripts.GameBase.GamePlay
{
    public class Player : BaseCharacter
    {
        [SerializeField] private List<Transform> bodyParts = new List<Transform>();
        [SerializeField] private Transform playerCenter;

        [HideIf(nameof(isBodySphere), Value = false)] [SerializeField]
        private ObiSolver obiSolver;

        [HideIf(nameof(isBodySphere), Value = false)] [SerializeField]
        private ObiRope obiRope;

        [SerializeField] private float bodyMoveSpeed = 5f;
        [SerializeField] private float minDistance = 1.5f;
        [SerializeField] private bool isBodySphere;

        private Transform _currentBodyPart;
        private Transform _previousBodyPart;
        private Vector3 _moveDirection;
        private Vector3 _center;

        private Collider[] _monsterCanKill;

        private float _distance;
        private float _radiusKillMonster;
        private bool _isKillMonster;

        #region Singleton

        public static Player Instance;

        protected override void Awake()
        {
            base.Awake();
            if (Instance == null) Instance = this;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Equals(Instance, this)) Instance = null;
        }

        #endregion

        private Vector3 target;

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            if (!GameManager.Instance.IsGameState(GameState.Playing) || !IsAlive) return;
            if (isBodySphere)
            {
                if (Physics.Raycast(Collider.bounds.center, transform.forward, out var hit, 0.1f,
                    1 << gameObject.layer))
                {
                    if (hit.collider.CompareTag("Player"))
                    {
                        var index = bodyParts.IndexOf(hit.transform);
                        FindMonster(bodyParts[index / 2 + 1].position, hit.transform.position);
                    }
                }
            }

            if (_isKillMonster && _monsterCanKill.Length > 0)
            {
                foreach (var x in _monsterCanKill)
                {
                    if (x != null && x.CompareTag("Monster") && _radiusKillMonster < 0.25f)
                    {
                        Destroy(x.gameObject);
                    }
                }
            }

            _moveDirection = Rigidbody.MoveDirection(
                transform,
                onMoving: () =>
                {
                    if (_radiusKillMonster >= 0)
                    {
                        _radiusKillMonster -= Time.deltaTime;
                    }
                    else
                    {
                        _isKillMonster = false;
                    }
                },
                onEnded: () =>
                {
                    if (isBodySphere) return;
                    for (var i = 1; i < obiRope.activeParticleCount; ++i)
                    {
                        var solverIndex = obiRope.solverIndices[i];
                        var prevSolverIndex = obiRope.solverIndices[i - 1];

                        Vector4 dir = Vector3.ProjectOnPlane(
                            obiSolver.positions[prevSolverIndex] - obiSolver.positions[solverIndex],
                            Vector3.zero).normalized;
                        obiSolver.velocities[solverIndex] += dir * (10f * Time.deltaTime);
                    }
                });

            if (!_moveDirection.Equals(Vector3.zero))
            {
                Rigidbody.MoveWithVelocity(_moveDirection, moveSpeed);
                transform.rotation = Quaternion.LookRotation(_moveDirection);
            }

            for (var i = 1; i < bodyParts.Count; i++)
            {
                _currentBodyPart = bodyParts[i];
                _previousBodyPart = bodyParts[i - 1];

                var position = _previousBodyPart.position;
                _distance = Vector3.Distance(position, _currentBodyPart.position);

                var newPosition = position;
                newPosition.y = bodyParts[0].position.y;

                var t = Time.fixedDeltaTime * _distance / minDistance * bodyMoveSpeed;
                if (t > 0.5f) t = 0.5f;
                if (_distance <= minDistance) return;

                _currentBodyPart.position = Vector3.Lerp(_currentBodyPart.position, newPosition, t);
                _currentBodyPart.rotation = Quaternion.Lerp(_currentBodyPart.rotation, _previousBodyPart.rotation, t);
            }
        }

        private void FindMonster(Vector3 a, Vector3 b, float radius = 2f)
        {
            _radiusKillMonster = Vector3.Distance(a, b) / radius;
            _center = (a + b) / 2;

            _monsterCanKill = Physics.OverlapSphere(_center, _radiusKillMonster);
            _isKillMonster = true;
        }

        protected override void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);
            if (other.CompareTag("PlayerEnd") && !isBodySphere)
            {
                FindMonster(transform.position, playerCenter.position, 3f);
            }
        }

        private void OnDrawGizmos()
        {
            if (_isKillMonster)
            {
                Gizmos.DrawWireSphere(_center, _radiusKillMonster);
            }
        }
    }
}